# Steps for JWT token for OAuth2.0

1. Client application registration
```
    Callback URL - http://localhost:8222/login/oauth2/code/okta
    Client-id - 0oa3mz4mtisXjRJf85d6
    Client-secret - X_Vsl5pK_RLAJ8P7lrYvqDYqUjFQL-oT7ReTxGuK
```

2. Authorizatin endpoint 
```
https://dev-7858070.okta.com/oauth2/default/v1/authorize?client_id=0oa3mz4mtisXjRJf85d6&response_type=code&scope=openid&redirect_uri=http%3A%2F%2Flocalhost%3A8222%2Flogin%2Foauth2%2Fcode%2Fokta&state=state-587bc94-a782-4a57-be1a-d0e2fd9bb601
```


3. Extract the Authorization code - mDlivsGjcN4nmOxgbjTP_m0giq14pKYh5aMnN3JOETs

4. Exchange `auth_code` for token from Auth server using `token` endpoint

```
curl --location --request POST 'https://dev-7858070.okta.com/oauth2/default/v1/token' \
--header 'accept: application/json' \
--header 'authorization: Basic MG9hM216NG10aXNYalJKZjg1ZDY6WF9Wc2w1cEtfUkxBSjhQN2xyWXZxRFlxVWpGUUwtb1Q3UmVUeEd1Sw==' \
--header 'content-type: application/x-www-form-urlencoded' \
--header 'Cookie: JSESSIONID=5FF186993A25A30E481F41B9EE87A475' \
--data-urlencode 'grant_type=authorization_code' \
--data-urlencode 'redirect_uri=http://localhost:8222/login/oauth2/code/okta' \
--data-urlencode 'code=ZcdHxeoc0jnQimjyvHzeFNzxn9bWjGXMYzRsDsw2tlQ'
```
