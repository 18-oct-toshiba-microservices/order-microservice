package com.classpath.ordermicroservice.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.AUTO;

@Data
@NoArgsConstructor
@Entity
@Table(name="orders")
@EqualsAndHashCode(of={"orderId", "price", "orderDate", "customerName"})
@ToString
@Builder
@AllArgsConstructor
public class Order {
    @Id
    @GeneratedValue(strategy = AUTO)
    private long orderId;
    private double price;
    private LocalDate orderDate;
    private String customerName;
    private LocalDateTime timestamp = LocalDateTime.now();
}