package com.classpath.ordermicroservice.util;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
@Log4j2
public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent> {
    private final OrderRepository orderRepository;
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        log.info("Inserting some records");
        IntStream.range(1, 10).forEach(index -> {
            Order order = Order.builder().orderDate(LocalDate.now()).price(Math.ceil(Math.random()* 1000)).customerName("ramesh "+index).build();
            // new Order(12, 45, 34000, "");
            this.orderRepository.save(order);
        });
    }
}