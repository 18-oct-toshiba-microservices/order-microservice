package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
@Slf4j
class DBHealthCheckEndpoint implements HealthIndicator, ApplicationListener<ApplicationReadyEvent> {
    private static int counter = 1;
    private static Health healthStatus = Health.outOfService().build();
    @Override
    public Health health() {
        log.info(" The counter value is : {}", System.currentTimeMillis());
        return healthStatus;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        healthStatus = Health.up().withDetail("DB","Application is up ").build();
    }
}