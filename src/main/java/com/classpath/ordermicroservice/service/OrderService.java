package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Log4j2
public class OrderService {

    private final OrderRepository orderRepository;
    private final RestTemplate restTemplate;

    private final KafkaTemplate<Long, Order> kafkaTemplate;

    //@CircuitBreaker(name="inventoryservice", fallbackMethod = "fallback")
    //@Retry(name="retryService", fallbackMethod = "fallback")
    @Transactional
    public Order save(Order order){
        log.info("About to update the inventory service :: {}");
       // final ResponseEntity<Integer> responseEntity = this.restTemplate
         //                                                   .postForEntity("http://inventory-service/api/v1/inventory", null, Integer.class);
        Order savedOrder = this.orderRepository.save(order);
        this.kafkaTemplate.send("orders-topic-new", order);

        return  savedOrder;
    }

    private Order fallback(Exception exception){
        log.error("Logging the exception :: {} ", exception.getMessage());
        return Order.builder().orderDate(LocalDate.now()).orderId(1111).customerName("naveen").build();
    }

    public Set<Order> fetchOrders(){
        return new HashSet<>(this.orderRepository.findAll());
    }

    public Order fetchByOrderId(long orderId){
        return this.orderRepository.findById(orderId)
                                    .orElseThrow( () -> new IllegalArgumentException("Invalid orderId"));
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }

    public void deleteOrders() {
        this.orderRepository.deleteAll();
    }
}